# OPA11 floorplan

Grundplan/indretning af OPA11 lokalet

Projektet er lavet med http://www.sweethome3d.com/ som er både open source og kører på en del platforme.

![sweethome3d](raw-sweethome3d.png)

## Note

* Undgå at modificere i eksistende tegninger med dit eget forslag, kopier i stedet en tegning.
* Lav en beskrivelse af din tegning i denne fil.

### Ønsker til indretning

* Vi skal have maksimalt lys, til det formål har vi fået foræret 30 stk. armaturer der er 170 cm lange.
* Vi skal have lyddæmpet lokalet.
* Dirty metal room: 4x4 m
* Dirty wood room: 4x4 m
* CNC cell: 2x3 m
* Laser Cell: 2x3 m
* En dedikeret placering til 3d printere (udsugning + brand-sikring), måske et stænk-skab?
* Elektronik bordene skal der findes plads til uden for dirty rooms.
* Køleskab
* Mindre mate lager inderst i rullereolen.
* Limbo
* Skabe til medlemmer???
* Arbejdsborde




## opa11-floorplan-raw.sh3d

* Dette er det næsten målfaste udgangspunkt for indretningen.
* De eneste ting der ikke er helt målfaste er størrelsen og placeringen af døre og lemme.
* Rullereolen er modelleret i "udfoldet" tilstand, så den kan ikke optage mere plads end den gør.
* Lad være med at modificere denne tegning, med mindre du har været ude for at måle flere detaljer op.

### Mangler

* Radiator
* Placering af eksisterende lyskontakt
* Placering af eksisterende lamper
* Placering af eksisterende eltavler
* Placering af eksisterende vandhane+gulvafløb

## opa11-floorplan-4rooms.sh3d

* Har 4 de fire rum til metal, træ, cnc og laser
* Har de to store elektronik borde
* Har samlet 12 stole med arbejdspladser, uden for dirty rooms.
* Al obevaring er samlet i rulle-helvedet.
* Har glas i alle døre, så det er nemmere at orientere sig.
* Har to store vinduer gennem dirty rooms, så man kan kigge gennem alle rummene fra indgangen.

### Pris

* 4x brugt terrasse dør: 2000 kr.
* 1x dobbelt terrasse dør: 1500 kr (den anden tager vi med fra KBV)
* 26 m gipsvæg med 96 mm reglar + glasuld og 1 lag gips per side ~ 10k
* Det løse: Skruer + spartel.

Totalt for rummene: ca. 15 k


